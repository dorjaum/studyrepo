package com.study.restapi.service;

import java.util.List;

import com.study.restapi.dto.LoginForm;
import com.study.restapi.dto.SomeBean;

public interface RestApiService {

	List<SomeBean> findAll();

	Integer createNew(LoginForm loginForm);

}
