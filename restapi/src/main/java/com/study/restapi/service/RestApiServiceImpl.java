package com.study.restapi.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.study.restapi.dto.LoginForm;
import com.study.restapi.dto.SomeBean;

@Service
public class RestApiServiceImpl implements RestApiService {

	@Override
	public List<SomeBean> findAll() {
		return Arrays.asList(new SomeBean());
	}

	@Override
	public Integer createNew(LoginForm loginForm) {
		return 1;
	}

}
