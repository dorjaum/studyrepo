package com.study.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.study.restapi.dto.LoginForm;
import com.study.restapi.dto.SomeBean;
import com.study.restapi.service.RestApiService;

@RestController
@RequestMapping("/restapi")
public class RestApiControllerImpl implements RestApiController {

	private RestApiService restApiService;

	@Autowired
	public RestApiControllerImpl(RestApiService restApiService) {
		this.restApiService = restApiService;
	}
	
	@GetMapping
	public List<SomeBean> findAll() {
		return restApiService.findAll();
	}
	
	@PostMapping
	public Integer createNew(@RequestBody LoginForm loginForm) {
		return restApiService.createNew(loginForm);
	}
}
