package com.study.restapi.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.study.restapi.service.RestApiService;

@RunWith(SpringRunner.class)
@WebMvcTest(RestApiControllerImpl.class)
public class RestApiControllerTest {

	private static final Integer ID_1 = 1;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RestApiService restApiService;

	@Test
	public void shouldReturnSuccess() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/restapi")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void shouldCreateNewObjectAndReturnId() throws Exception {
		String jsonBody = "{\"user\":\"user\", \"password\": \"password\"}";
		Mockito.when(restApiService.createNew(Mockito.any())).thenReturn(ID_1);
		
		mockMvc.perform(MockMvcRequestBuilders
					.post("/restapi")
					.content(jsonBody)
					.contentType("application/json"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(ID_1.toString()));
	}
}
