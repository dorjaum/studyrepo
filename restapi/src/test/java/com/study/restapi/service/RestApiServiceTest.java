package com.study.restapi.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.study.restapi.dto.LoginForm;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RestApiServiceTest {

	@Autowired
	private RestApiService restApiService;
	
	@Test
	public void shouldReturnIdAfterPost() {
		LoginForm loginForm = new LoginForm();
		Integer newId = restApiService.createNew(loginForm);
		
		Integer expectedId = 1;
		Assert.assertEquals(expectedId, newId);
	}
}
