package restapi;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.study.restapi.config.PersistenceConfig;
import com.study.restapi.config.RestApiConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		classes = { RestApiConfiguration.class}, 
		loader = AnnotationConfigContextLoader.class)
public class SpringContextIntegrationTest {

}
